package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

public interface UserService {

    //Create a User
    void createUser(User user);

    // Viewing all users
    Iterable<User> getUsers();

    //Delete a user
    ResponseEntity deleteUser(Long id);

    //Update a user
    ResponseEntity updateUser(Long id, User user);

}
